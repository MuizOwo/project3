import React, { Component}from 'react';
import './App.css';
import Home from './Component/Home';
import AddInfo from './Component/AddInfo';


class App extends Component {

  render(){
  return (
    <div className="App">
                            {/* Navbar */}
        <nav className="navbar navbar-light bg-dark">
            <a className="navbar-brand"><i className="fab fa-affiliatetheme "></i>ZOLA</a>
              <form className="form-inline">
                <button className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">Login</button>
                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Start your Registry</button>
              </form>
          </nav>
                          {/* Discount page */}
          <nav className="navbar navbar-light bg-light">
            <h5 className='center'>
                GET $25 to spend at zola plus 20% OFF post wedding when you sign up today
            </h5>
          </nav>
                                {/* Home Page */}
                      <Home />
                                {/*Login info*/}
                      <AddInfo  />   
      </div>
  );
}
}

export default App;
