import React, { Component } from 'react'
import '../App.css'


export class AddInfo extends Component {
           render() {
                      return (
                        <form className='bg-dark'>
                        <div className="form-group">
                          <label for="exampleInputEmail1">Email address</label>
                          <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                        </div>
                        <div className="form-group">
                          <label for="exampleInputPassword1">Password</label>
                          <input type="password" className="form-control" id="exampleInputPassword1"/>
                        </div>
                        <div className="form-group form-check">
                          <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                          <label className="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <div className='B bg-light'>
                        <button type="submit" className="btn">Submit</button>
                        </div>
                      </form>
                      )
           }
}

export default AddInfo
